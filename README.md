# Business Data Analytics Exercises SS24

This repository contains the exercise materials for the BDA lecture.

Should you have any questions, feel free to contact [David Borukhson](mailto:david.borukhson@kit.edu) or [Alexander Grote](mailto:alexander.grote@kit.edu)